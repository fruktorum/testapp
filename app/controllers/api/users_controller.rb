module Api
	class UsersController < ApplicationController
		before_action :authenticate_user!

		def index
			render json: User.all
		end

		def role
			current_role = User.roles[ params[ :role ] ]
			render json: User.where( role: current_role )
		end

		def inactive
			render json: User.where( active: false )
		end
	end
end
