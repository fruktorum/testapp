class window.Router
	test = (url) ->
		uri = url.match( /https?\:\/\/.+?([\/$].*)/ )[ 1 ]

		switch true
			when uri.length <= 1
				'Главная'
			when /^\/api\/users$/.test uri
				'Все пользователи'
			when /^\/api\/users\/inactive$/.test uri
				'Неактивные пользователи'
			when /^\/api\/users\/by_role/.test uri
				if uri.match( /by_role\/(.+)/ )[ 1 ] == 'admin'
					'Администраторы'
				else
					'Пользователи'
			else
				'404'

	@updateTitle: (path) ->
		$( 'h1.page-title' ).html test path

$ -> window.Router.updateTitle window.location.href
