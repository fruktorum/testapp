$ ->
	generateUsersTable = (users) ->
		table = $( '<table></table>' ).addClass 'users'
		table.append '<tr><th class="id">id</th><th class="email">Email</th></tr>'

		fields = [ 'id', 'email' ]
		for user in users
			row = $ '<tr></tr>'
			row.append $( '<td></td>' ).text user[ field ] for field in fields
			table.append row

		$( '.index' ).html table
		$( '.index' ).append 'Empty!' unless users.length

	$( '.sidebar .link.xhr' ).click (event) ->
		do event.preventDefault

		xhr = new XMLHttpRequest
		xhr.open 'GET', event.target.href, true
		xhr.onreadystatechange = ->
			if xhr.readyState == 4
				window.Router.updateTitle event.target.href

				users = JSON.parse xhr.response
				generateUsersTable users

		do xhr.send
