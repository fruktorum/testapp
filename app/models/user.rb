class User < ActiveRecord::Base
	devise :database_authenticatable, :registerable
	enum role: %w[ user admin ]
end
