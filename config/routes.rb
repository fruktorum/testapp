Rails.application.routes.draw do
	root 'home#index'
	get :home, controller: :home, action: :index

	devise_for :users

	namespace :api, format: :json do
		resources :users, only: :index do
			collection do
				get :role, path: '/by_role/:role'
				get :inactive
			end
		end
	end
end
